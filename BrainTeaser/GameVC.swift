//
//  GameVC.swift
//  BrainTeaser
//
//  Created by Stanislav Sidelnikov on 29/06/16.
//  Copyright © 2016 Stanislav Sidelnikov. All rights reserved.
//

import UIKit
import pop

class GameVC: UIViewController {
    @IBOutlet weak var yesBtn: CustomButton!
    @IBOutlet weak var noBtn: CustomButton!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var timerLbl: UILabel!
    private var checkmarkIV: CheckmarkImageView!

    class func getViewForMark() -> UIImageView {
        let view = UIImageView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        view.contentMode = .ScaleAspectFit
        return view
    }
    private lazy var correctIconIV: UIImageView = {
        let view = GameVC.getViewForMark()
        view.image = UIImage(named: "correct_mark")
        return view
    }()
    private lazy var incorrectIconIV: UIImageView = {
        let view = GameVC.getViewForMark()
        view.image = UIImage(named: "incorrect_mark")
        return view
    }()

    private var game: Game!
    private var currentCard: Card!
    private var timer: NSTimer!
    static let gamePeriod = 60
    private var timeRemained = gamePeriod {
        didSet {
            let (h,m,s) = secondsToHoursMinutesSeconds(timeRemained)
            var time = String.init(format: "%02d:%02d", m, s)
            if h > 0 {
                time = String.init(format: "%02d:%@", h, time)
            }
            timerLbl.text = time
        }
    }
    private var gameRunning = false

    override func viewDidLoad() {
        super.viewDidLoad()

        checkmarkIV = CheckmarkImageView(frame: CGRect(origin: CGPointZero, size: CGSize(width: 100, height: 100)))
        checkmarkIV.contentMode = .ScaleAspectFit
        checkmarkIV.layer.zPosition = 1
        self.view.addSubview(checkmarkIV)

        initializeGame()
    }

    @IBAction func yesPressed(sender: UIButton) {
        if gameRunning {
            checkAnswer(true)
        } else {
            startGame()
        }
        showNextCard()
    }

    @IBAction func noPressed(sender: UIButton) {
        checkAnswer(false)
        showNextCard()
    }

    private func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }

    private func startGame() {
        timeRemained = GameVC.gamePeriod
        timerLbl.hidden = false
        timer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: #selector(timerFired), userInfo: nil, repeats: true)
        gameRunning = true
        titleLbl.text = "Does this card match the previous?"
    }

    private func stopGame() {
        performSegueWithIdentifier("ShowResults", sender: self)
        timer.invalidate()
        gameRunning = false
        noBtn.enabled = false
        yesBtn.enabled = false
    }

    private func initializeGame() {
        titleLbl.text = "Remember this image"
        timerLbl.hidden = true
        noBtn.hidden = true
        noBtn.enabled = true
        yesBtn.enabled = true
        yesBtn.setTitle("START", forState: .Normal)
        game = Game(shapesCount: Card.shapes.count)

        if currentCard != nil {
            currentCard.removeFromSuperview()
        }
        currentCard = createCardFromNib()
        currentCard.center = AnimationEngine.screenCenterPosition
        self.view.addSubview(currentCard)
    }

    func timerFired() {
        timeRemained -= 1
        if timeRemained == 0 {
            stopGame()
        }
    }

    private func checkAnswer(match: Bool) {
        if game.checkAnswer(match: match) {
            checkmarkIV.image = UIImage(named: "correct_mark")
        } else {
            checkmarkIV.image = UIImage(named: "incorrect_mark")
        }
        checkmarkIV.showTemporarily()
    }

    private func showViewTemporary(view: UIView) {
        let fadeInAnimation = POPBasicAnimation(propertyNamed: kPOPViewAlpha)
        fadeInAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        fadeInAnimation.fromValue = 0
        fadeInAnimation.toValue = 0
    }

    private func showNextCard() {
        if let current = currentCard {
            let cardToRemove = current
            currentCard = nil
            AnimationEngine.animateToPosition(cardToRemove, position: AnimationEngine.offScreenLeftPosition, completion: { (anim: POPAnimation!, finised: Bool) in
                cardToRemove.removeFromSuperview()
            })
        }
        if let next = createCardFromNib() {
            next.center = AnimationEngine.offScreenRightPosition
            self.view.addSubview(next)
            currentCard = next

            if noBtn.hidden {
                noBtn.hidden = false
                yesBtn.setTitle("YES", forState: .Normal)
            }

            let cardFrame = next.frame
            let position = AnimationEngine.screenCenterPosition
            let checkSize = checkmarkIV.bounds.size
            var x = position.x + cardFrame.size.width / 2 - checkSize.width / 2
            var y = position.y + cardFrame.size.height / 2 - checkSize.height / 2
            let k = CheckmarkImageView.scaleFactor
            x = min(x, CGRectGetMaxX(UIScreen.mainScreen().bounds) - checkSize.width * k)
            y = min(y, CGRectGetMaxY(UIScreen.mainScreen().bounds) - checkSize.height * k)
            self.checkmarkIV.frame.origin = CGPoint(x: x, y: y)
            AnimationEngine.animateToPosition(next, position: AnimationEngine.screenCenterPosition, completion: { (anim: POPAnimation!, finished: Bool) in
            })
        }
    }

    private func createCardFromNib() -> Card! {
        let shapeNumber = game.nextShape()
        let card = NSBundle.mainBundle().loadNibNamed("Card", owner: self, options: nil)[0] as? Card
        card!.setShapeNumber(shapeNumber)
        return card
    }

    @IBAction func restartGame(segue: UIStoryboardSegue) {
        initializeGame()
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.destinationViewController.isKindOfClass(ResultsVC) {
            let controller = segue.destinationViewController as! ResultsVC
            controller.score = game.score
            controller.correctCount = game.correctCount
            controller.incorrectCount = game.incorrectCount
        }
    }
}
