//
//  CheckmarkImageView.swift
//  BrainTeaser
//
//  Created by Stanislav Sidelnikov on 06/07/16.
//  Copyright © 2016 Stanislav Sidelnikov. All rights reserved.
//

import UIKit

@IBDesignable
class CheckmarkImageView: UIImageView {
    static let scaleFactor: CGFloat = 1.3

    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }

    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setupView()
    }

    private func setupView() {
        self.alpha = 0.0
    }

    func showTemporarily() {
        self.alpha = 0.0
        self.transform = CGAffineTransformIdentity
        UIView.animateWithDuration(0.3, animations: {
            self.alpha = 1.0
            self.transform = CGAffineTransformScale(self.transform, CheckmarkImageView.scaleFactor, CheckmarkImageView.scaleFactor)
            }) { (success) in
                if success {
                    UIView.animateWithDuration(0.2, delay: 0.2, options: [], animations: {
                        self.alpha = 0.0
                    }) { (success) in
                        if success {
                            self.transform = CGAffineTransformIdentity
                        }
                    }
                }
        }
    }
}
