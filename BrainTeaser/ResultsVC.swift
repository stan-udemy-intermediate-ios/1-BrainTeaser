//
//  ResultsVC.swift
//  BrainTeaser
//
//  Created by Stanislav Sidelnikov on 06/07/16.
//  Copyright © 2016 Stanislav Sidelnikov. All rights reserved.
//

import UIKit

class ResultsVC: UIViewController {
    @IBOutlet weak var resultLbl: UILabel!
    @IBOutlet weak var correctCountLbl: UILabel!
    @IBOutlet weak var incorrectCountLbl: UILabel!

    var score = 0
    var correctCount = 0
    var incorrectCount = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        resultLbl.text = "\(score)"
        correctCountLbl.text = "\(correctCount)"
        incorrectCountLbl.text = "\(incorrectCount)"
    }

}
