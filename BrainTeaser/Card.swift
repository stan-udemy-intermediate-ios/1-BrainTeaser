//
//  Card.swift
//  BrainTeaser
//
//  Created by Stanislav Sidelnikov on 29/06/16.
//  Copyright © 2016 Stanislav Sidelnikov. All rights reserved.
//

import UIKit

class Card: UIView {

    static let shapes = ["shape1", "shape2", "shape3"]
    var currentShape: String!

    @IBOutlet weak var shapeImage: UIImageView!

    @IBInspectable var cornerRadius: CGFloat = 3.0 {
        didSet {
            setupView()
        }
    }

    override func prepareForInterfaceBuilder() {
        setupView()
    }

    override func awakeFromNib() {
        setupView()
    }

    func setupView() {
        self.layer.cornerRadius = cornerRadius
        self.layer.shadowOpacity = 0.8
        self.layer.shadowRadius = 5.0
        self.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.layer.shadowColor = UIColor(red: 157.0/255.0, green: 157.0/255.0, blue: 157.0/255.0, alpha: 1.0).CGColor
        self.setNeedsLayout()
    }

    func setShapeNumber(number: Int) {
        currentShape = Card.shapes[number]
        shapeImage.image = UIImage(named: currentShape)
    }
}
