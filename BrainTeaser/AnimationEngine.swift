//
//  AnimationEngine.swift
//  BrainTeaser
//
//  Created by Stanislav Sidelnikov on 29/06/16.
//  Copyright © 2016 Stanislav Sidelnikov. All rights reserved.
//

import UIKit
import pop

class AnimationEngine {
    class var offScreenRightPosition: CGPoint {
        return CGPoint(x: UIScreen.mainScreen().bounds.width, y: CGRectGetMidY(UIScreen.mainScreen().bounds))
    }

    class var offScreenLeftPosition: CGPoint {
        return CGPoint(x: -UIScreen.mainScreen().bounds.width, y: CGRectGetMidY(UIScreen.mainScreen().bounds))
    }

    class var screenCenterPosition: CGPoint {
        return CGPoint(x: CGRectGetMidX(UIScreen.mainScreen().bounds),
                       y: CGRectGetMidY(UIScreen.mainScreen().bounds))
    }

    let ANIM_DELAY: Double = 0.8
    var originalConstants = [CGFloat]()
    var constraints: [NSLayoutConstraint]!

    init(constraints: [NSLayoutConstraint]) {
        for con in constraints {
            originalConstants.append(con.constant)
            con.constant = AnimationEngine.offScreenRightPosition.x
        }

        self.constraints = constraints
    }

    func animateOnScreen(delay: Double?) {
        let d: Int64 = Int64((delay == nil ? ANIM_DELAY : delay!) * Double(NSEC_PER_SEC))
        let time = dispatch_time(DISPATCH_TIME_NOW, d)

        dispatch_after(time, dispatch_get_main_queue()) {
            var index = 0
            repeat {
                let moveAnim = POPSpringAnimation(propertyNamed: kPOPLayoutConstraintConstant)
                moveAnim.toValue = self.originalConstants[index]
                moveAnim.springBounciness = 12
                moveAnim.springSpeed = 12

                if (index > 0) {
                    moveAnim.dynamicsFriction += 15 + CGFloat(index)
                }

                let con = self.constraints[index]
                con.pop_addAnimation(moveAnim, forKey: "moveOnScreen")

                index += 1
            } while index < self.constraints.count
        }
    }

    class func animateToPosition(view: UIView, position: CGPoint, completion: ((POPAnimation!, Bool) -> Void)) {
        let moveAnim = POPSpringAnimation(propertyNamed: kPOPLayerPosition)
        moveAnim.toValue = NSValue(CGPoint: position)
        moveAnim.springBounciness = 8
        moveAnim.springSpeed = 8
        moveAnim.completionBlock = completion
        view.pop_addAnimation(moveAnim, forKey: "moveToPosition")
    }
}