//
//  Game.swift
//  BrainTeaser
//
//  Created by Stanislav Sidelnikov on 06/07/16.
//  Copyright © 2016 Stanislav Sidelnikov. All rights reserved.
//

import Foundation

class Game {
    let shapesCount: Int
    private var currentShape: Int?
    private var previousShape: Int?
    var score: Int { return max(correctCount * scoreIncrease - incorrectCount * scoreDecrease, 0) }
    private(set) var correctCount = 0
    private(set) var incorrectCount = 0
    private let scoreIncrease = 1
    private let scoreDecrease = 1

    init(shapesCount count: Int) {
        shapesCount = count
    }

    func nextShape() -> Int {
        previousShape = currentShape
        currentShape = Int(arc4random_uniform(UInt32(shapesCount)))
        return currentShape!
    }

    func checkAnswer(match match: Bool) -> Bool {
        let correct = (previousShape == currentShape) == match
        if correct {
            correctCount += 1
        } else {
            incorrectCount += 1
        }
        return correct
    }
}